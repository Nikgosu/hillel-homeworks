let stationery = [
	{
		'name': 'pen1',
		'price': 2,
		'category': 'pens'
	},
	{
		'name': 'pencil1',
		'price': 1,
		'category': 'pensils'
	},
	{
		'name': 'pencil case',
		'price': 3,
		'category': 'pencil cases'
	},
	{
		'name': 'diary',
		'price': 4,
		'category': 'diary\'s'
	},
	{
		'name': 'corrector',
		'price': 2,
		'category': 'correctors'
	},
	{
		'name': 'pencil2',
		'price': 2,
		'category': 'pensils'
	},
	{
		'name': 'pen2',
		'price': 3,
		'category': 'pens'
	},
	{
		'name': 'pen1',
		'price': 2,
		'category': 'pens'
	},
	{
		'name': 'pencil1',
		'price': 1,
		'category': 'pensils'
	},
	{
		'name': 'pencil case',
		'price': 3,
		'category': 'pencil cases'
	},
	{
		'name': 'diary',
		'price': 4,
		'category': 'diary\'s'
	},
	{
		'name': 'corrector',
		'price': 2,
		'category': 'correctors'
	},
	{
		'name': 'pencil2',
		'price': 2,
		'category': 'pensils'
	},
	{
		'name': 'pen2',
		'price': 3,
		'category': 'pens'
	}
]

function getNewStationery(arr) {
	let confirmNewStationery = confirm('Хотите добавить новый товар?')
	if (confirmNewStationery === true) {
		let nameStationery;
		let price;
		let category;
		let confirmNewStationery1
		do {
			nameStationery = prompt('Введите название товара!');
			do {
				price = +prompt('Введите цену товара!');
			} while (!isFinite(parseFloat(price)));
			category = prompt('Введите категорию товара!');
			confirmNewStationery1 = confirm('Хотите добавить ещё один товар?')
			if (nameStationery !== null && price !== null && category !== null) {
				arr.push({
					'name': nameStationery,
					'price': price,
					'category': category
				})
			}
		} while (confirmNewStationery1 === true)
		return arr;
	}
	return arr
}

function getFiltredPrice(arr) {
	let arrEmpty = [];
	let confirmFiltred = confirm('Хотите отфильтровать по цене?')
	if (confirmFiltred === true) {
		let lowerPrice;
		let upperPrice;
		do {
			lowerPrice = +prompt('Введите нижнюю цену');
			upperPrice = +prompt('Введите верхнюю цену');
		} while (!isFinite(parseFloat(lowerPrice)) && !isFinite(parseFloat(upperPrice)))
		return arr.filter(stationery => stationery['price'] >= lowerPrice && stationery['price'] <= upperPrice);
	}
	return arrEmpty;
}

function getFiltredCategory(arr) {
	let arrEmpty = [];
	let confirmFiltred = confirm('Хотите отфильтровать по категории?')
	if (confirmFiltred === true) {
		let category;
		do {
			category = prompt('Введите категорию на латиннице');
		} while (typeof category !== 'string')
		return arr.filter(stationery => stationery['category'] === category);
	}
	return arrEmpty;
}

function getNumberCoincidences(arr) {
	let arrEmpty = [];
	let NumberCoincidences
	let confirmNumberCoincidences = confirm('Хотите посчитать кол-во товаров в категории?')
	if (confirmNumberCoincidences === true) {
		let category;
		do {
			category = prompt('Введите категорию на латиннице');
		} while (typeof category !== 'string')
		NumberCoincidences = arr.filter(x => x['category'] == category).length;
		return NumberCoincidences;
	}
	return arrEmpty;
}

function getWithoutStationery(arr) {
	let arrEmpty = [];
	let WithoutStationery
	let confirmNumberCoincidences = confirm('Хотите удалить товар по имени?')
	if (confirmNumberCoincidences === true) {
		let nameStationery;
		do {
			nameStationery = prompt('Введите название товара на латиннице');
		} while (typeof nameStationery !== 'string')
		WithoutStationery = arr.filter(item => item['name'] !== nameStationery);
		return console.log(`Канцтовары без ${nameStationery}`, WithoutStationery);
	}
	return arrEmpty;
}

function getSortedDescending(arr) {
	let concatArr = arr.concat()
	let sortedArr = concatArr.sort((a, b) => b.price - a.price);
	return sortedArr;
}

function getSortedAscending(arr) {
	let concatArr = arr.concat()
	let sortedArr = concatArr.sort((a, b) => a.price - b.price);
	return sortedArr;
}

function getSortAndFilter(arr) {
	let filterType;
	let sortingType;
	let filterTypeCategory;
	let filterTypePrice
	let confirmSortAndFilter = confirm('Хотите выбрать тип сортировки и фильтр?')
	if (confirmSortAndFilter === true) {
		filterType = prompt('Фильтр');
		if (filterType === 'по категории') {
			filterTypeCategory = getFiltredCategory(arr)
			sortingType = prompt('Тип сортировки');
			if (sortingType === 'по-убыванию') {
				return console.log('Отсортировано по категории и по-убыванию', getSortedDescending(filterTypeCategory))
			} else if (sortingType === 'по-возрастанию') {
				return console.log('Отсортировано по категории и по-возрастанию', getSortedAscending(filterTypeCategory));
			}
		} else if (filterType === 'по цене') {
			filterTypePrice = getFiltredPrice(arr)
			sortingType = prompt('Тип сортировки');
			if (sortingType === 'по-убыванию') {
				return console.log('Отсортировано по цене и по-убыванию', getSortedDescending(filterTypePrice))
			} else if (sortingType === 'по-возрастанию') {
				return console.log('Отсортировано по цене и по-возрастанию', getSortedAscending(filterTypePrice));
			}
		}
	}
}

function getPriceFilterAndCategory(arr) {
	let filterTypeCategory;
	let filterTypePrice;
	let filterType;
	let filterTypeCategorySum;
	let confirmPriceFilterAndCategory = confirm('Хотите выбрать фильтр?')
	if (confirmPriceFilterAndCategory === true) {
		filterType = prompt('Фильтр');
		if (filterType === 'по категории') {
			filterTypeCategory = getFiltredCategory(arr)
			filterTypeCategorySum = filterTypeCategory.reduce(function (prev, curr) { return prev + curr.price }, 0);
			return console.log('Сумма цен товаров отфильтрованных по категории', filterTypeCategorySum);
		} else if (filterType === 'по цене') {
			filterTypePrice = getFiltredPrice(arr)
			filterTypePriceSum = filterTypePrice.reduce(function (prev, curr) { return prev + curr.price }, 0);
			return console.log('Сумма цен товаров отфильтрованных по цене', filterTypePriceSum);
		}
	}
}

let arrWithNewStationery = getNewStationery(stationery);
let filtredByPrice = getFiltredPrice(stationery);
let filtredByCategory = getFiltredCategory(stationery);
let numberCoincidencesStationery = getNumberCoincidences(stationery);
let withoutStationeryList = getWithoutStationery(stationery)
let sortedDescending = getSortedDescending(arrWithNewStationery);
let sortedAscending = getSortedAscending(sortedDescending);

console.log('Массив с новым товаром', arrWithNewStationery);
console.log('Отфильтрованный по цене', filtredByPrice);
console.log('Отфильтрованный по категории', filtredByCategory);
console.log('Получено кол-во товаров в категории', numberCoincidencesStationery);
console.log('Массив без выбранного товара', withoutStationeryList);
console.log('Отсортировано по-убыванию цены', sortedDescending);
console.log('Отсортировано по-возрастанию цены', sortedAscending);
getSortAndFilter(arrWithNewStationery);
getPriceFilterAndCategory(arrWithNewStationery);















