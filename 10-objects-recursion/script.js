let obj = {
	a: 'f',
	b: 78,
	c: 'R',
	d: {
		a: {
			a: null,
			b: 'E',
			c: {
				a: true,
				b: 'C',
				c: 'test'
			},
			d: 'U'
		},
		b: {
			a: 'R',
			b: ['S', 4, 6, 'I'],
			c: 0,
		},
		c: ['O'],
		d: null,
		e: 'N'
	}
}

let finnalString = []

const getUpperCaseLetter = (obj) => {

	for (let key in obj) {
		if (typeof obj[key] === 'string') {
			if (obj[key] === obj[key].toUpperCase()) {
				finnalString.push(obj[key])
			}
		} else if (typeof obj[key] === 'object') {
			getUpperCaseLetter(obj[key])
		} else if (Array.isArray(obj[key])) {
			getUpperCaseLetter(obj[key])
		}
	}
}

getUpperCaseLetter(obj)

console.log(finnalString.join(''))

// function getFinalString(obj) {
// 	let upperString = '';
// 	let upperLetter;
// 	if (Array.isArray(obj)) {
// 		obj.forEach(function (item) {
// 			upperLetter = getUpperCase(obj[item]);
// 			if (upperLetter) upperString += upperLetter;
// 		})
// 	}
// 	for (let i in obj) {
// 		if (obj[i] instanceof Object && obj[i] !== null) {
// 			upperLetter = getFinalString(obj[i]);
// 			if (upperLetter) upperString += upperLetter;
// 			continue;
// 		}
// 		upperLetter = getUpperCase(obj[i]);
// 		if (upperLetter) upperString += upperLetter;
// 	}
// 	return upperString;
// }
//
// function getType() {
//
// }
//
// function getUpperCase(value1) {
// 	let upperLetter;
// 	if (typeof value1 === `string`) {
// 		for (let value of Object.values(value1)) {
// 			objKey = value;
// 			if (objKey === objKey.toUpperCase()) {
// 				upperLetter = objKey;
// 				return upperLetter;
// 			}
// 		}
// 	}
// 	return false;
// }
//
// let obj = {
// 	a: 'f',
// 	b: 78,
// 	c: 'R',
// 	d: {
// 		a: {
// 			a: null,
// 			b: 'E',
// 			c: {
// 				a: true,
// 				b: 'C',
// 				c: 'test'
// 			},
// 			d: 'U'
// 		},
// 		b: {
// 			a: 'R',
// 			b: ['S', 4, 6, 'I'],
// 			c: 0,
// 		},
// 		c: ['O'],
// 		d: null,
// 		e: 'N'
// 	}
// }
//
// let finalString = getFinalString(obj);
//
// console.log(obj);
// console.log(finalString);