let oldExchangeRates  = 2772.4 / 100;
let newExchangeRates;
let someMoney;
let exchangeDifference;

do {
	someMoney = +prompt('Введите сумму:');
} while (!isFinite(parseFloat(someMoney)));

do {
	newExchangeRates = +prompt('Введите курс:');
} while (!isFinite(parseFloat(newExchangeRates)));

exchangeDifference = someMoney * (newExchangeRates - oldExchangeRates);

if ((someMoney * oldExchangeRates) < (someMoney * newExchangeRates)) {
	console.log(`Положительная курсовая разница: ${exchangeDifference} uah`);
} else {
	console.log(`Отрицательная курсовая разница: ${exchangeDifference} uah`);
}





