'use strict';

!(function () {
  function createElement({
    tagName = 'div',
    classes = [],
    dataAttributes = {},
    textContent = '',
  }) {
    if (typeof tagName !== 'string') {
      console.warn('tagName createElement method of app must be string');
      let errorElement = document.createElement('div');
      errorElement.textContent =
        'tagName createElement method of app must be string';
      return errorElement;
    }

    let element = document.createElement(tagName);

    if (typeof textContent === 'string') {
      element.textContent = textContent;
    } else {
      console.warn('textContent createElement method of app must be string');
    }

    if (Array.isArray(classes)) {
      classes.forEach((className) => {
        if (typeof className === 'string') {
          element.classList.add(className);
        } else {
          console.warn(
            'classes element of app createElement method must be string'
          );
        }
      });
    }

    if (typeof dataAttributes === 'Object' && dataAttributes) {
      Object.entries(dataAttributes).forEach((pair) => {
        if (typeof pair[0] !== 'string' || typeof pair[1] !== 'string') {
          element.setAttribute(pair[0], pair[1]);
        } else {
          console.warn(
            'dataAttributes element of app createElement method must be string'
          );
        }
      });
    }
    return element;
  }

  class Phonebook {
    constructor() {
      this.contactsArr = [];
      this.init = function () {
        _initApp();
      };

      let _body = document.querySelector('body');
      let _phoneField;
      let _nameField;
      let _contactsBlock;
      let _createContact = () => {
        let contactTitle = _nameField.value;
        let contactText = _phoneField.value;
        let textFieldStates = [];
        textFieldStates.push(_validateTextFields(_nameField));
        textFieldStates.push(_validateTextFields(_phoneField));

        if (textFieldStates.some((state) => state === false)) {
          return;
        }

        let isExist = this.contactsArr.some(
          (contact) => contact.title === contactTitle
        );
        let isCreate;

        if (isExist) {
          isCreate = confirm(
            'Do you have such a contact, do you want to make one more?'
          );
          if (!isCreate) {
            return;
          }
        }

        let contact = new Contact({ contactTitle, contactText });
        this.contactsArr.push(contact);

        let contactsStates = this.contactsArr.map((contact) => {
          return {
            title: contact.title,
            text: contact.text,
          };
        });
        localStorage.setItem('contacts', JSON.stringify(contactsStates));

        _contactsBlock.append(contact.element);
      };
      let _initApp = () => {
        let appBlock = createElement({ classes: ['container'] });
        let tittle = createElement({
          tagName: 'h1',
          textContent: 'Phonebook',
        });
        let createcontactButton = createElement({
          tagName: 'button',
          classes: ['btn', 'btn-primary'],
          textContent: 'Create contact',
        });
        _contactsBlock = createElement({
          classes: ['container', 'contacts-block'],
        });

        _nameField = createElement({
          tagName: 'input',
          classes: ['form-control'],
          dataAttributes: { placeholder: 'Name', autocomplete: 'autocomplete' },
        });
        _phoneField = createElement({
          tagName: 'input',
          classes: ['form-control'],
          dataAttributes: { placeholder: 'Name', autocomplete: 'autocomplete' },
        });

        appBlock.append(
          tittle,
          _nameField,
          _phoneField,
          createcontactButton,
          _contactsBlock
        );
        _body.append(appBlock);

        createcontactButton.addEventListener('click', _createContact);

        _getContacts();
      };

      let _getContacts = () => {
        let contactsJSON = localStorage.getItem('contacts');
        if (contactsJSON) {
          let contactsData = JSON.parse(contactsJSON);
          this.contactsArr = contactsData.map((contactData) => {
            return new Contact({
              contactTitle: contactData.title,
              contactText: contactData.text,
            });
          });

          this.contactsArr.forEach((contact) => {
            _contactsBlock.append(contact.element);
          });
        }
      };
      let _validateTextFields = (field) => {
        if (field.value === '') {
          field.classList.add('is-invalid');
          return false;
        } else {
          field.classList.remove('is-invalid');
          return true;
        }
      };
    }
  }

  class Contact {
    constructor({ contactTitle = '', contactText = '' }) {
      let _deleteButton;

      let _createElement = () => {
        let contactElement = createElement({ classes: 'contact' });
        let contactTitleElement = createElement({
          tagName: 'h5',
          classes: ['contact-tittle'],
          textContent: contactTitle,
        });
        let contactTextElement = createElement({
          tagName: 'p',
          classes: ['contact-text'],
          textContent: contactText,
        });

        let controlsContainer = createElement({
          classes: ['controls-container'],
        });

        let updateButton = createElement({
          tagName: 'button',
          classes: ['btn', 'btn-primary'],
          textContent: 'Update contact',
        });
        _deleteButton = createElement({
          tagName: 'button',
          classes: ['btn', 'btn-primary'],
          textContent: 'Delete contact',
        });
        let importanceCheckbox = createElement({
          tagName: 'input',
          classes: ['form-check-input'],
          dataAttributes: { type: 'checkbox', id: 'importanceCheckbox' },
        });
        let importanceCheckboxLabel = createElement({
          tagName: 'label',
          classes: ['form-check-label'],
          dataAttributes: { for: 'importanceCheckbox' },
          textContent: 'Important',
        });

        controlsContainer.append(
          updateButton,
          _deleteButton,
          importanceCheckbox,
          importanceCheckboxLabel
        );
        contactElement.append(
          contactTitleElement,
          contactTextElement,
          controlsContainer
        );

        return contactElement;
      };

      let _element = _createElement();

      this.title = contactTitle;
      this.text = contactText;
      this.element = _element;

      let _atachEvents = () => {
        _deleteButton.addEventListener('click', _deleteContact);
      };

      let _deleteContact = () => {
        _element.remove();
      };

      _atachEvents();
    }
  }

  let app = new Phonebook();
  app.init();
})();
