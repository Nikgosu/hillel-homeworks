'use strict';
function _isNumber(number) {
  return !isNaN(parseFloat(number)) && isFinite(number);
}
function _isBoolean(boolean) {
  if (typeof boolean === 'boolean') {
    return true;
  } else {
    return false;
  }
}

class Auto {
  constructor() {
    this._startInterval;
    this._consumption = 2.4;
    this._tankVolume = 60;
    this.gasVolume = 0;
    this.ignition = false;
    this.speed;
    this.distance;
    this.speedConsumption;
    this.finnalyConsumption;
    this.color;
  }
  _checkGasInput = (isFull, gasVolume) => {
    if (!_isBoolean(isFull)) {
      console.warn('isFull addGas function parameter must be a Boolean');
      return false;
    }
    if (!isFull && !_isNumber(gasVolume)) {
      console.warn('gasVolume addGas function parameter must be a Number');
      return false;
    } else {
      return true;
    }
  };
  _checkGasVolume = (gasVolume) => {
    return (
      this.gasVolume + gasVolume < this._tankVolume &&
      this.gasVolume + this.gasVolume >= 0
    );
  };
  _checkGasExisting = () => {
    if (this.gasVolume >= 0) {
      return true;
    } else {
      console.warn('Fuel not found');
    }
  };
  _checkIgnition = () => {
    if (this.ignition === true) {
      console.log('ignition already on');
      return true;
    }
  };
  _carIgnition = () => {
    if (this._checkIgnition()) {
      return;
    } else {
      this.ignition = true;
    }
  };
  _ride = () => {
    this._carIgnition();
    this.speedConsumption = (this.speed * this._consumption) / 50;
    this.finnalyConsumption = (this.distance * this.speedConsumption) / 100;
    this._startInterval = setInterval(() => {
      if (this.gasVolume.toFixed(2) > 0) {
        if (this._checkGasExisting() && this.finnalyConsumption) {
          this.gasVolume -= this.finnalyConsumption;
        } else {
          this.stop();
          console.warn('You are looser');
        }
      } else {
        clearInterval(this._startInterval);
        console.log(
          `Fuel ran out, you passed ${
            this.distance
          } km, you were driving at a speed of ${
            this.speed
          } km per hour, your fuel consumption was ${
            this.finnalyConsumption
          } liters per second, you have ${
            this.gasVolume + this.finnalyConsumption
          } liters of gasoline left`
        );
      }
    }, 1000);
  };
  checkGas = () => {
    return this.gasVolume.toFixed(2);
  };
  addGas = (isFull = false, gasVolume = 0) => {
    if (!this._checkGasInput(isFull, gasVolume)) {
      return;
    }
    if (isFull) {
      this.gasVolume = this._tankVolume;
    } else {
      if (this._checkGasVolume()) {
        this.gasVolume += this.gasVolume;
      } else {
        console.warn(
          `gas volume is to large. You can add ${
            this._tankVolume - this.gasVolume
          }`
        );
      }
    }
  };
  start = () => {
    this._carIgnition();
    this._startInterval = setInterval(() => {
      if (this._checkGasExisting() && !this.finnalyConsumption) {
        this.gasVolume -= this._consumption;
      } else {
        this.stop();
        console.warn('You are looser');
      }
    }, 1000);
  };
  stop = () => {
    this.ignition = false;
    clearInterval(this._startInterval);
  };
  ride = (distanceCar, speedCar) => {
    if (!_isNumber(speedCar) || !_isNumber(distanceCar)) {
      return console.log('distance and speed must be number!');
    }
    this.speed = speedCar;
    this.distance = distanceCar;
    this._ride();
  };
  stopRide = () => {
    this.ignition = false;
    console.log(
      `End ride, you passed ${
        this.distance
      } km, you were driving at a speed of ${
        this.speed
      } km per hour, your fuel consumption was ${
        this.finnalyConsumption
      } liters per second, you have ${
        this.gasVolume + this.finnalyConsumption
      } liters of gasoline left`
    );
    clearInterval(this._startInterval);
  };
}
