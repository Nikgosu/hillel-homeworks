const isStringOrArray = (source) => {
	if (typeof source === 'string' | Array.isArray(source)) return true
}

const utills = {
	reverse (source) {
		if (!isStringOrArray(source)) return
		if (typeof source === 'string') {
			return source.split('').reverse().join('')
		} else {
			return source.reverse()
		}
	},
	sort (source) {
		if (!isStringOrArray(source)) return
		if (typeof source === 'string') {
			return source.split('').sort((a, b) => a - b)
		} else {
			return source.sort((a, b) => a - b)
		}
	},
	verifyNumbers (source) {
		if (!Array.isArray(source)) return
		return source.filter(item => !isNaN(Number(item)))
	},
	getMin (source) {
		if (!Array.isArray(source)) return
		return source.reduce((a, b) => Math.min(a, b))
	}
}

console.log(utills.reverse('sdafs'))
console.log(utills.sort([1,5,3,2,8,0]))
console.log(utills.verifyNumbers(['12332']))
console.log(utills.getMin(1))

