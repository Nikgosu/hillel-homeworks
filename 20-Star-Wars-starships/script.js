'use strict';

!(function () {
    class Planets {
        constructor() {
            this.body = document.querySelector('body');
            this.init = function () {
                this._init();
            }
        }
        _init = () => {
            this._createElements()
            this._attachEvents()
        }
        _isString = (str) => {
            return typeof str === 'string';
        }
        _createElement = ({
                          tagName = 'div',
                          classes = [],
                          dataAttributes = {},
                          textContent = '',
                      }) => {
            if (!this._isString(tagName)) {
                console.warn('tagName createElement method of app must be string');
                let errorElement = document.createElement('div');
                errorElement.textContent =
                    'tagName createElement method of app must be string';
                return errorElement;
            }

            let element = document.createElement(tagName);

            if (this._isString(textContent)) {
                element.textContent = textContent;
            } else {
                console.warn('textContent createElement method of app must be string');
            }

            if (Array.isArray(classes)) {
                classes.forEach((className) => {
                    if (this._isString(className)) {
                        element.classList.add(className);
                    } else {
                        console.warn(
                            'classes element of app createElement method must be string'
                        );
                    }
                });
            }

            if (typeof dataAttributes === 'object' && dataAttributes) {
                Object.entries(dataAttributes).forEach((pair) => {
                    if (this._isString(pair[0]) || this._isString(pair[1])) {
                        element.setAttribute(pair[0], pair[1]);
                    } else {
                        console.warn(
                            'dataAttributes element of app createElement method must be string'
                        );
                    }
                });
            }
            return element;
        }
        _createElements = () => {

            this.appBlock = this._createElement({
                classes: ['container']
            })
            let tittle = this._createElement({
                tagName: 'h1',
                textContent: 'Planets from Star Wars'
            });

            this.planetsBlock = this._createElement({
                classes: ['container__planets-block']});
            this.planetTable = this._createElement({tagName: 'table', classes: ['table']})
            this.planetsTBody = this._createElement({tagName: 'tbody', classes: ['table']})
            this.button = this._createElement({
                tagName: 'button',
                classes: ['btn', 'btn-primary', 'btn-lg'],
                textContent: 'Get planets'
            });
            let tr = this._createElement({tagName: 'tr', classes: ['planet-tr']});
            let thName = this._createElement({tagName: 'th', textContent: 'Name'})
            let thClimate = this._createElement({tagName: 'th', textContent: 'Climate'})
            let thCreated = this._createElement({tagName: 'th', textContent: 'Created'})
            let thDiameter = this._createElement({tagName: 'th', textContent: 'Diameter'})
            let thEdited = this._createElement({tagName: 'th', textContent: 'Edited'})
            let thGravity = this._createElement({tagName: 'th', textContent: 'Gravity'})
            let thOrbitalPeriod = this._createElement({tagName: 'th', textContent: 'OrbitalPeriod'})
            let thPopulation = this._createElement({tagName: 'th', textContent: 'Population'})
            let thRotationPeriod = this._createElement({tagName: 'th', textContent: 'Rotation Period'})
            let thSurfaceWater = this._createElement({tagName: 'th', textContent: 'Surface Water'})
            let thTerrain = this._createElement({tagName: 'th', textContent: 'Terrain'})
            tr.append(thName, thClimate, thCreated, thDiameter, thEdited,
                thGravity, thOrbitalPeriod, thPopulation, thRotationPeriod,
                thSurfaceWater, thTerrain)
            this.planetsTBody.append(tr);
            this.planetTable.append(this.planetsTBody);
            this.planetsBlock.append(this.planetTable);
            this.appBlock.append(tittle, this.button, this.planetsBlock);
            this.body.append(this.appBlock);
        }
        _getPlanetsElements = (xhrArr) => {
            if (!this.planetsArr) {
                this.planetsArr = xhrArr;
                this.planetsArr.forEach(({
                                             climate,
                                             created,
                                             diameter,
                                             edited,
                                             gravity,
                                             name,
                                             orbital_period,
                                             population,
                                             rotation_period,
                                             surface_water,
                                             terrain
                                         }) => {
                    let planetTr = this._createElement({tagName: 'tr', classes: ['planet']});
                    let planetName = this._createElement({tagName: 'th', textContent: name})
                    let planetClimate = this._createElement({tagName: 'th', textContent: climate})
                    let planetCreated = this._createElement({tagName: 'th', textContent: created})
                    let planetDiameter = this._createElement({tagName: 'th', textContent: diameter})
                    let planetEdited = this._createElement({tagName: 'th', textContent: edited})
                    let planetGravity = this._createElement({tagName: 'th', textContent: gravity})
                    let orbitalPeriod = this._createElement({tagName: 'th', textContent: orbital_period})
                    let planetPopulation = this._createElement({tagName: 'th', textContent: population})
                    let rotationPeriod = this._createElement({tagName: 'th', textContent: rotation_period})
                    let surfaceWater = this._createElement({tagName: 'th', textContent: surface_water})
                    let planetTerrain = this._createElement({tagName: 'th', textContent: terrain})

                    planetTr.append(planetName, planetClimate, planetTerrain, planetPopulation, planetCreated, planetGravity, planetEdited, planetDiameter, rotationPeriod, surfaceWater, orbitalPeriod);
                    this.planetsTBody.append(planetTr);
                })
            } else {
                console.log('We already get planets!)))')}
        }
        _getPlanets = () => {

            let xhr = new XMLHttpRequest();

            xhr.open('GET', 'https://swapi.dev/api/planets')
            xhr.responseType = 'json';
            xhr.send()

            xhr.onload = () => {
                if (xhr.status !== 200) {
                    console.log(`Error ${xhr.status}: ${xhr.statusText}`)
                    console.log(xhr.status);
                }
                if (xhr.response !== null) {
                    try {
                        this._getPlanetsElements(xhr.response.results);
                    } catch(err) {
                        console.log(`Error load data: ${err}`)
                    }
                }
            };

            xhr.onerror = function() {
                console.log((`Connection error`));
            };
        }
        _attachEvents = () => {
            this.button.addEventListener('click', this._getPlanets);
        }
    }
    let planets = new Planets();
    planets.init();
})();
