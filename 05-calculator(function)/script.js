let history = [];
let index;
let a;
let b;

function promptIndex() {
	do {
		index = +prompt(`
		Choose an operation (put the number): 
		1 - sum
		2 - dif
		3 - mult
		4 - div
		5 - pow
		6 - sin
		7 - cos
		8 - history
		`);
	} while (index > 8 || index < 1 || isNaN(index));
}

function promptA() {
	if (index < 8) {
		do {
			a = +prompt('Insert first operand: ', '0');
		} while (isNaN(a));
	}
}

function promptB() {
	if (index < 6) {
		do {
			b = +prompt('Insert second operand: ', '0');
		} while (isNaN(b));
	}
}

function sum() {
	let resultFunc = `sum: ${a} + ${b} = ${a + b}`;
	return resultFunc;
}
function dif() {
	let resultFunc = `dif: ${a} - ${b} = ${a - b}`;
	return resultFunc;
}
function mult() {
	let resultFunc = `mult: ${a} * ${b} = ${a * b}`;
	return resultFunc;
}
function div() {
	let resultFunc = `div: ${a} / ${b} = ${a / b}`;
	return resultFunc;
}
function pow() {
	let resultFunc = `pow: ${a} ** ${b} = ${a ** b}`;
	return resultFunc;
}
function sin() {
	let resultFunc = `sin: sin(${a}) = ${Math.sin(a)}`;
	return resultFunc;
}
function cos() {
	let resultFunc = `cos: cos(${a}) = ${Math.cos(a)}`;
	return resultFunc;
}

do {
	alert('START')

	promptIndex();

	promptA();

	promptB();

	let result = ('The result of operation: ');

	if (index == 1) {
		result += sum(a, b);
		console.log(result);
	} else if (index == 2) {
		result += dif(a, b);
		console.log(result);
	} else if (index == 3) {
		result += mult(a, b);
		console.log(result);
	} else if (index == 4) {
		result += div(a, b);
		console.log(result);
	} else if (index == 5) {
		result += pow(a, b);
		console.log(result);
	} else if (index == 6) {
		result += sin(a, b);
		console.log(result);
	} else if (index == 7) {
		result += cos(a, b);
		console.log(result);
	} else if (index == 8) {
		console.log(history);
	}
	if (index > 0 && index < 8) {
		history[history.length] = result;
	}
} while (confirm('Do you wanna repeat?'));

