let history = [];

do {
	let index;
	let a;
	let b;

	do {
		index = +prompt(`
		Choose an operation (put the number): 
		1 - sum
		2 - dif
		3 - mult
		4 - div
		5 - pow
		6 - sin
		7 - cos
		8 - history
		`);
	} while (index > 8 || index < 1 || isNaN(index));

	if (index < 8) {
		do {
			a = +prompt('Insert first operand: ', '0');
		} while (isNaN(a));}

	if (index < 6) {
		do {
			b = +prompt('Insert second operand: ', '0');
		} while (isNaN(b));
	}


	let result = ('The result of operation: ');

	if (index == 1) {
		result += `sum: ${a} + ${b} = ${a + b}`;
		console.log(result);
	} else if (index == 2) {
		result += `dif: ${a} - ${b} = ${a - b}`;
		console.log(result);
	} else if (index == 3) {
		result += `mult: ${a} * ${b} = ${a * b}`;
		console.log(result);
	} else if (index == 4) {
		result += `div: ${a} / ${b} = ${a / b}`;
		console.log(result);
	} else if (index == 5) {
		result += `pow: ${a} ** ${b} = ${a ** b}`;
		console.log(result);
	} else if (index == 6) {
		result += `sin: sin(${a}) = ${Math.sin(a)}`;
		console.log(result);
	} else if (index == 7) {
		result += `cos: cos(${a}) = ${Math.cos(a)}`;
		console.log(result);
	} else {
		console.log(history);
	}
	if (index > 0 && index < 8) {
		history[history.length] = result;
	}
} while (confirm('Do you wanna repeat?'));

