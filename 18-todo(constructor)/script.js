'use strict';

!(function () {
  function createElement({
    tagName = 'div',
    classes = [],
    dataAttributes = {},
    textContent = '',
  }) {
    if (typeof tagName !== 'string') {
      console.warn('tagName createElement method of app must be string');
      let errorElement = document.createElement('div');
      errorElement.textContent =
        'tagName createElement method of app must be string';
      return errorElement;
    }

    let element = document.createElement(tagName);

    if (typeof textContent === 'string') {
      element.textContent = textContent;
    } else {
      console.warn('textContent createElement method of app must be string');
    }

    if (Array.isArray(classes)) {
      classes.forEach((className) => {
        if (typeof className === 'string') {
          element.classList.add(className);
        } else {
          console.warn(
            'classes element of app createElement method must be string'
          );
        }
      });
    }

    if (typeof dataAttributes === 'Object' && dataAttributes) {
      Object.entries(dataAttributes).forEach((pair) => {
        if (typeof pair[0] !== 'string' || typeof pair[1] !== 'string') {
          element.setAttribute(pair[0], pair[1]);
        } else {
          console.warn(
            'dataAttributes element of app createElement method must be string'
          );
        }
      });
    }
    return element;
  }

  class App {
    constructor() {
      this.cardsArr = [];
      this.init = function () {
        this._init();
      };

      this._body = document.querySelector('body');
      this._descriptionField;
      this._nameField;
      this._cardsBlock;
    }
    _getCards = () => {
      let cardsJSON = localStorage.getItem('cards');
      if (cardsJSON) {
        let cardsData = JSON.parse(cardsJSON);
        this.cardsArr = cardsData.map((cardData) => {
          return new Card({
            cardTitle: cardData.title,
            cardText: cardData.text,
          });
        });

        this.cardsArr.forEach((card) => {
          this._cardsBlock.append(card.element);
        });
      }
    };
    _init = () => {
      let appBlock = createElement({ classes: ['container'] });
      let tittle = createElement({
        tagName: 'h1',
        textContent: 'Awesome TODO app',
      });
      let createCardButton = createElement({
        tagName: 'button',
        classes: ['btn', 'btn-primary'],
        textContent: 'Create card',
      });
      this._cardsBlock = createElement({
        classes: ['container', 'cards-block'],
      });

      this._nameField = createElement({
        tagName: 'input',
        classes: ['form-control'],
        dataAttributes: { placeholder: 'Name', autocomplete: 'autocomplete' },
      });
      this._descriptionField = createElement({
        tagName: 'textarea',
        classes: ['form-control'],
        dataAttributes: { placeholder: 'Name', autocomplete: 'autocomplete' },
      });

      appBlock.append(
        tittle,
        this._nameField,
        this._descriptionField,
        createCardButton,
        this._cardsBlock
      );
      this._body.append(appBlock);

      createCardButton.addEventListener('click', this._createCard);
      this._getCards();
    };

    _createCard = () => {
      let cardTitle = this._nameField.value;
      let cardText = this._descriptionField.value;
      let textFieldStates = [];
      textFieldStates.push(this._validateTextFields(this._nameField));
      textFieldStates.push(this._validateTextFields(this._descriptionField));

      if (textFieldStates.some((state) => state === false)) {
        return;
      }

      let isExist = this.cardsArr.some((card) => card.title === cardTitle);
      let isCreate;

      if (isExist) {
        isCreate = confirm(
          'Do you have such a card, do you want to make one more?'
        );
        if (!isCreate) {
          return;
        }
      }

      let card = new Card({ cardTitle, cardText });
      this.cardsArr.push(card);

      let cardsStates = this.cardsArr.map((card) => {
        return {
          title: card.title,
          text: card.text,
        };
      });
      localStorage.setItem('cards', JSON.stringify(cardsStates));

      this._cardsBlock.append(card.element);
    };

    _validateTextFields = (field) => {
      if (field.value === '') {
        field.classList.add('is-invalid');
        return false;
      } else {
        field.classList.remove('is-invalid');
        return true;
      }
    };
  }

  class Card {
    constructor({ cardTitle = '', cardText = '' }) {
      this.title = cardTitle;
      this.text = cardText;
      this.element = this._createElement();
    }
    _createElement = () => {
      let cardElement = createElement({ classes: 'card' });
      let cardTitleElement = createElement({
        tagName: 'h5',
        classes: ['card-tittle'],
        textContent: this.title,
      });
      let cardTextElement = createElement({
        tagName: 'p',
        classes: ['card-text'],
        textContent: this.text,
      });

      let controlsContainer = createElement({
        classes: ['controls-container'],
      });

      let updateButton = createElement({
        tagName: 'button',
        classes: ['btn', 'btn-primary'],
        textContent: 'Update card',
      });
      this._deleteButton = createElement({
        tagName: 'button',
        classes: ['btn', 'btn-primary'],
        textContent: 'Delete card',
      });
      let importanceCheckbox = createElement({
        tagName: 'input',
        classes: ['form-check-input'],
        dataAttributes: { type: 'checkbox', id: 'importanceCheckbox' },
      });
      let importanceCheckboxLabel = createElement({
        tagName: 'label',
        classes: ['form-check-label'],
        dataAttributes: { for: 'importanceCheckbox' },
        textContent: 'Important',
      });

      controlsContainer.append(
        updateButton,
        this._deleteButton,
        importanceCheckbox,
        importanceCheckboxLabel
      );
      cardElement.append(cardTitleElement, cardTextElement, controlsContainer);
      this._atachEvents();
      return cardElement;
    };

    _atachEvents = () => {
      this._deleteButton.addEventListener('click', this._deleteCard);
    };

    _deleteCard = () => {
      this.element.remove();
    };
  }

  let app = new App();
  console.log();
  app.init();
})();

let cards;
