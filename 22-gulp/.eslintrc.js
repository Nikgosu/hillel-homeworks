module.exports =
    {
      'env': {
        'browser': true,
        'commonjs': true,
      },
      "parser": "@babel/eslint-parser",
      "parserOptions": {
        "ecmaVersion": 8,
          "requireConfigFile": false,
        "sourceType": "module",
        "ecmaFeatures": {
          "jsx": true,
          "modules": true,
          "experimentalObjectRestSpread": true
        }
      },
      "plugins": [
        "react"
      ],
      "extends": ["google", "eslint:recommended", "plugin:react/recommended"],
      "rules": {
        "comma-dangle": 0,
        "react/jsx-uses-vars": 1,
        "react/display-name": 1,
        "no-unused-vars": "warn",
        "no-console": 1,
        "no-unexpected-multiline": "warn"
      },
      "settings": {
        "react": {
          "pragma": "React",
          "version": "15.6.1"
        }
      }
    }

