const gulp = require('gulp');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const eslint = require('gulp-eslint')

gulp.task('scripts', () => {
    return gulp.src('*.js')
	    .pipe(eslint({
		    fix: true
	    }))
	    .pipe(eslint.format())
	    .pipe(eslint.failAfterError())
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('dest'));
});

