'use strict';

const icon = document.querySelector('#icon');
const mandarinsAmountElement = document.querySelector('#mandarinsAmount');
const startButton = document.querySelector('#startButton');
const restartButton = document.querySelector('#restartButton');
const gameTime = document.querySelector('#gameTime');
const mandarin = document.querySelector('#mandarin');
const grinch = document.querySelector('#grinch');
const mandarinWidth = parseInt(getComputedStyle(mandarin).width);
const mandarinHeight = parseInt(getComputedStyle(mandarin).height);
const gameDifficultyBlock = document.querySelector('.game-difficulty');
let gameCanvasWidth = document.querySelector('.game-canvas').offsetWidth;
let gameCanvasHeight = document.querySelector('.game-canvas').offsetHeight;
let canvasSizeLeft = 100 - (mandarinWidth / gameCanvasWidth * 100);
let canvasSizeTop = 100 - (mandarinHeight / gameCanvasHeight * 100);
let easyDifficulty = 1000;
let normalDifficulty = 750;
let hardDifficulty = 500;
let gameDifficultyInMilliseconds;


let gameInterval;
let mandarinsAmount = 0;
let gameDuration = 15;
let mandarinInterval;
let grinchIntervalDelay;

mandarin.addEventListener('click', event => {
	mandarinsAmount++;
	mandarinsAmountElement.textContent = `You caught ${mandarinsAmount} mandarins`;
	setMandarinPosition();
	clearInterval(mandarinInterval);
	mandarinInterval = setInterval(setMandarinPosition, 1000);
});

grinch.addEventListener('click', () => {
	gameDuration = 15;
	gameTime.textContent = `Game over!`;
	icon.style.display = 'none';
	clearInterval(mandarinInterval);
	clearInterval(gameInterval);
	mandarinsAmount--;
	mandarinsAmountElement.textContent = `You caught ${mandarinsAmount} mandarins`;
	startButton.removeAttribute('disabled');
});

function resetGame() {
	gameDuration = 15;
	mandarinsAmount = 0;
	mandarinsAmountElement.textContent = `You caught ${mandarinsAmount} mandarins`;
	gameTime.textContent = `${gameDuration} seconds left`
}

function setMandarinPosition() {
	icon.style.left = Math.round(Math.random() * canvasSizeLeft) + '%';
	icon.style.top = Math.round(Math.random() * canvasSizeTop) + '%';
	if (gameDuration === grinchIntervalDelay) {
		mandarin.style.display = 'none';
		grinch.style.display = 'block';
		grinchIntervalDelay = Math.round(Math.random() * gameDuration);
	} else {
		mandarin.style.display = 'block';
		grinch.style.display = 'none';
	}
}

function gameCount() {
	if (gameDuration === 1) {
		icon.style.display = 'none';
		gameTime.textContent = `Game over!`;
		clearInterval(gameInterval);
		clearInterval(mandarinInterval);
		startButton.removeAttribute('disabled');
		return;
	}

	gameDuration -= 1;
	gameTime.textContent = `${gameDuration} seconds left`;
}

function startGame() {
	startButton.setAttribute('disabled', 'disabled');
	setMandarinPosition();
	icon.style.display = 'block';
	resetGame();
	grinchIntervalDelay = Math.round(Math.random() * gameDuration);
	mandarinInterval = setInterval(setMandarinPosition, gameDifficultyInMilliseconds);
	gameInterval = setInterval(gameCount, 1000);
}

gameDifficultyBlock.addEventListener('click', event => {
	let easyButton = document.querySelector('#easyButton');
	let normalButton = document.querySelector('#normalButton');
	let hardButton = document.querySelector('#hardButton');
	if (event.currentTarget === gameDifficultyBlock) {
		startButton.style.display = 'block';
		gameDifficultyBlock.style.display = 'none';
		if (event.target === easyButton) {
			gameDifficultyInMilliseconds = easyDifficulty;
		} else if (event.target === normalButton) {
			gameDifficultyInMilliseconds = normalDifficulty;
		} else if (event.target === hardButton) {
			gameDifficultyInMilliseconds = hardDifficulty;
		}
	}
})
restartButton.addEventListener('click', event => {
	resetGame();
	icon.style.display = 'none';
	gameDifficultyBlock.style.display = 'block';
	startButton.style.display = 'block';
	startButton.removeAttribute('disabled', 'disabled');
	clearInterval(mandarinInterval);
	clearInterval(gameInterval);
});

startButton.addEventListener('click', event => {
	startGame()
});









