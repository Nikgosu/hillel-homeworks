import React, {FC, useContext, useEffect, useId, useState} from 'react';
import {Button, Card, CardActions, CardContent, Checkbox, FormControlLabel, Typography} from "@mui/material";
import {ICard} from "../types/types";
import {MyContext} from "../context/MyContext";

interface Props {
  card: ICard
  setLS: () => void
  deleteCard: (deletedCard: ICard) => void
}

const CardItem:FC<Props> = ({card, setLS, deleteCard}: Props) => {

  const [important, setImportant] = useState(card.isImportant)
  const [done, setDone] = useState(card.isDone)

  const contextValue = useContext(MyContext)

  const handleCell = () => {
    contextValue?.handleContextCard(card)
  }

  useEffect(() => {
    card.isDone ? card.isDone = false : card.isDone = true
    setLS()
  }, [card, setLS, done])

  useEffect(() => {
    card.isImportant ? card.isImportant = false : card.isImportant = true
    setLS()
  }, [card, setLS, important])

  return (
    <Card key={useId()} sx={{
      width: '100%',
      marginTop: '20px',
      border: important ? '5px solid aquamarine' : '1px solid lightgray'
    }}>
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {card.info.name}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {card.info.description}
        </Typography>
      </CardContent>
      <CardActions>
        <Button
          onClick={handleCell}
          disabled={'disabled' && done}
          variant='contained'
          size="medium"
        >Update card
        </Button>
        <Button
          disabled={'disabled' && done}
          onClick={() => deleteCard(card)}
          variant="outlined" size="medium" color="error">Delete card</Button>
        <FormControlLabel
          disabled={'disabled' && done}
          sx={{ marginLeft: '5px' }}
          control={<Checkbox checked={important} onChange={(e) => setImportant(e.target.checked)}/>}
          label="Important" />
        <Button
          onClick={() => setDone(prev => !prev)}
          variant='contained'
          size="small"
          color={done ? 'error' : "success"} >{done ? 'Not done' : 'Done'}</Button>
      </CardActions>
    </Card>
  );
};

export default CardItem;