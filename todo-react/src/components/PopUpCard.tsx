import React, {FC, useEffect, useState} from 'react';
import {ICard} from "../types/types";
import {Button, Container, Grid, TextField} from "@mui/material";

const PopUpCard:FC = () => {

  const [card, setCard] = useState<ICard>()
  const [editedCard, setEditedCard] = useState<{ info: { name: string; description: string; }; id?: number | undefined; isImportant?: boolean | undefined; isDone?: boolean | undefined }>()
  const [nameValue, setNameValue] = useState('')
  const [descriptionValue, setDescriptionValue] = useState('')

  const getCardByMessage = (e: MessageEvent) => {
    if (e.data.message === 'OK!') setCard(e.data.value)
  }

  const updateCard = () => {
    setEditedCard({...card, info: {name: nameValue, description: descriptionValue}})
    setTimeout(() => {window.close()}, 50)
  }

  const handleUpdateByEnter = (e: React.KeyboardEvent) => {
    if (e.key === 'Enter') updateCard()
  }

  useEffect(() => {
    window.addEventListener('message', getCardByMessage)
  }, [])

  useEffect(() => {
    if (card) {
      setNameValue(card.info.name)
      setDescriptionValue(card.info.description)
    }
  }, [card])

  useEffect(() => {
    window.opener.postMessage({message: 'OK!', value: editedCard}, '*')
  }, [editedCard])

  return (
    <Container style={{
      marginTop: '40px',
      width: '90%'
    }}>
      <Grid style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
      }}>
        <TextField
          onKeyPress={handleUpdateByEnter}
          value={nameValue}
          onChange={(e) => setNameValue(e.target.value)}
          placeholder='Name'
          style={{width: '100%'}}/>
      </Grid>
      <Grid style={{
        marginTop: '20px',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
      }}>
        <TextField
          onKeyPress={handleUpdateByEnter}
          value={descriptionValue}
          onChange={(e) => setDescriptionValue(e.target.value)}
          placeholder='Description'
          style={{width: '100%'}}/>
      </Grid>
      <Grid style={{
        marginTop: '20px',
      }}>
        <Button onClick={updateCard} variant='contained'>Update card</Button>
      </Grid>
    </Container>
  );
};

export default PopUpCard;