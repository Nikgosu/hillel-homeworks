export interface ICard {
  id: number
  info: {
    name: string,
    description: string
  }
  isImportant: boolean
  isDone: boolean
}
