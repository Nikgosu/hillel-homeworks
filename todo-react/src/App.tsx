import React, {useEffect, useState} from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './App.css';
import Home from "./pages/Home";
import ErrorPage from "./pages/ErrorPage";
import NavBar from "./components/NavBar";
import About from "./pages/About";
import {ICard} from "./types/types";
import PopUpCard from "./components/PopUpCard";
import {MyContext} from "./context/MyContext"

function App() {

  const [isNew, setIsNew] = useState(false)
  const [editedCard, setEditedCard] = useState<ICard>()
  const [isEdited, setIsEdited] = useState(false)
  const [win, setWin] = useState<Window | null>()
  const [contextCard, setContextCard] = useState<ICard>()

  const handleContextCard = (card: ICard) => {
    setContextCard(card)
  }

  const logEditedCard = (e: MessageEvent) => {
    if (e.data.message === 'OK!' && e.data.value !== undefined) {
      setEditedCard(e.data.value)
    }
  }

  useEffect(() => {
    if (contextCard) {
      if (!Object.keys(contextCard).length) return
      setIsNew(true)
    }
  }, [contextCard])

  useEffect(() => {
    if (contextCard !== undefined && editedCard !== undefined) {
      contextCard.info.name = editedCard.info.name
      contextCard.info = editedCard.info
      setIsEdited(prev => !prev)
    }
  }, [contextCard, editedCard])

  useEffect(() => {
    if (isNew) {
      setWin(window.open(`/popup/:${contextCard?.id}`, 'pop-up', 'width=800,height=800'))
      setIsNew(false)
    }
  }, [contextCard, isNew])

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      win && win.postMessage({message: 'OK!', value: contextCard}, '*')
    }, 200)
    return () => clearTimeout(timeoutId)
  }, [win, contextCard])

  useEffect(() => {
    window.addEventListener('message', logEditedCard)
  }, [])

  return (
    <BrowserRouter>
      <NavBar/>
      <MyContext.Provider value={{contextCard, handleContextCard}}>
        <Routes>
          <Route path='/' element={<Home/>}/>
          <Route path='/about' element={<About/>}/>
          <Route path={`/popup/:${contextCard?.id}`} element={<PopUpCard/>}/>
          <Route path='*' element={<ErrorPage/>}/>
        </Routes>
      </MyContext.Provider>
    </BrowserRouter>
  );
}

export default App;
