import React, {FC, useCallback, useEffect, useState} from 'react';
import { Container, SelectChangeEvent } from "@mui/material";
import CreateCardFrom from "../components/CreateCardFrom";
import {ICard} from "../types/types";
import {nanoid} from "nanoid";
import CardItem from "../components/CardItem";
import SortSelect from "../components/SortSelect";

const getId = () => {
  return nanoid()
}

const Home:FC = () => {
  const [cards, setCards] = useState<ICard[]>(JSON.parse(localStorage.getItem('cards') ?? '[]'))
  const [sort, setSort] = useState<number>(10)


  const setLS = useCallback(() => {
    localStorage.setItem('cards', JSON.stringify(cards))
  }, [cards])

  const addCard = useCallback((card: ICard) => {
    setCards([...cards, card])
  }, [cards])

  const deleteCard = (deletedCard: ICard) => {
    setCards(cards.filter(card => deletedCard.id !== card.id))
  }

  const handleChangeSort = (e: SelectChangeEvent<number>) => {
    setSort(+e.target.value)
    Number(e.target.value) === 10 ? cards.sort((a, b) => a.info.name < b.info.name ? 1 : -1) : cards.sort((a, b) => a.info.name > b.info.name ? 1 : -1)
  }

  useEffect(() => {
    setLS()
  }, [cards, setLS])


  return (
    <>
      <CreateCardFrom addCard={addCard}/>
      <Container style={{
        marginTop: '40px',
        width: '90%'
      }}>
        <SortSelect sort={sort} handleChangeSort={handleChangeSort}/>
        {
          cards.map(card => (
            <CardItem key={getId()} setLS={setLS} card={card} deleteCard={deleteCard}/>
          ))
        }
      </Container>
    </>
  );
};

export default Home;