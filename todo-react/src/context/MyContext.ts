import {createContext} from "react";
import {ICard} from "../types/types";

interface IContext {
  contextCard: ICard | undefined
  handleContextCard: (card: ICard) => void
}

export const MyContext = createContext<IContext | null>(null)