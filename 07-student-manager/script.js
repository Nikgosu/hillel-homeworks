const studentsArr = studentsMock.getStudentList(50)

const setAverageMark = (arr) => {
	arr.map(student => {
		student.averageMark = student.marks.reduce((prevMark, currentMark) => prevMark + currentMark) / student.marks.length
	})
	return arr
}

const getLooserList = (arr) => {
	return arr.filter(student => student.averageMark < 50)
}

const setMedianMark = (arr) => {
	arr.map(student => {
		student.medianMark = student.marks.filter((item, i, arr) => arr.indexOf(item) !== i);
	})
	return arr
}

const createNewStudent = (arr) => {
	arr.push(studentsMock.getStudent())
}

const sortByAverageRating = (arr) => {
	arr.sort((a, b) => a.averageMark > b.averageMark ? -1 : b.averageMark > a.averageMark ? 1 : 0)
	return arr
}

const setResultArr = (arr) => {
	const resultArr = []
	arr.forEach(student => {
		resultArr.push({name: student.name, averageMark: student.averageMark})
	})
	return resultArr
}

setAverageMark(studentsArr)
setMedianMark(studentsArr)
sortByAverageRating(studentsArr)

console.log(studentsArr)
console.log(getLooserList(studentsArr))
console.table(setResultArr(studentsArr))